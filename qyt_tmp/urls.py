"""qyt_tmp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from views import index, qyt_template, qyt_db, qyt_charts, qyt_rpc, qyt_login
from django.contrib.auth.views import LoginView, LogoutView



urlpatterns = [
    path('admin/', admin.site.urls),
    path('summary/', qyt_template.summary),
    path('sec/', qyt_template.sec_course),
    path('dc/', qyt_template.dc_course),
    # 简单修改数据
    path('dbchange/', qyt_db.InsertData),
    # 查询并展示所有学员信息
    path('dbshowstudents/', qyt_db.showstudents),
    # 添加学员信息表单
    path('dbaddstudent/', qyt_db.addstudent),
    # 删除特定学员信息
    path('deletestudent/<int:id>/', qyt_db.deletestudent),
    # 编辑学员信息
    path('editstudent/<int:id>/', qyt_db.editstudent),
    # 图表测试主页(iframe刷新页面)
    path('charts/', qyt_charts.charts),
    # 图表测试主页(iframe使用JSON刷新数据)
    path('chartsjson/', qyt_charts.chartsjson),
    # 特定图表类型,特定刷新周期,特定设备的chart(刷新页面)
    path('chartsupdate/<str:chart_type>/<str:period>/<int:deviceid>/', qyt_charts.charts_update),
    # 特定图表类型,特定刷新周期,特定设备的chart(JSON刷新数据)
    path('chartsupdatejson/<str:chart_type>/<str:period>/<int:deviceid>/', qyt_charts.charts_update_json),
    # 不推荐刷新页面方式,刷新的页面
    path('chartsdata/<str:chart_type>/<int:deviceid>/', qyt_charts.charts_data),
    # 推荐的JSON刷新数据方式,提供JSON数据的页面
    path('chartjson/<str:chart_type>/<int:deviceid>/', qyt_rpc.chart_json),
    # FusionCharts展示页面
    path('fusioncharts/', qyt_charts.fusioncharts),
    # rpc测试提供时间JSON数据的页面
    path('rpcdatetime/', qyt_rpc.rpc_datetime),
    # rpc测试提供实时刷新时间数据的页面
    path('rpcshow/', qyt_rpc.rpc_show),
    # 直接在Charts主页,使用JSON获取数据
    path('chartsjsonnoiframe/', qyt_rpc.charts_json_noiframe),
    # 登录页面
    path('accounts/login/', qyt_login.qyt_login),
    # 注销页面
    path('accounts/logout/', qyt_login.qyt_logout),

    # 主页
    path('', index.index)
]
