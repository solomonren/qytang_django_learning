#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from qytdb.models import StudentsDB
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from qytdb.forms import StudentsForm, EditStudents
from django.contrib.auth.decorators import login_required, permission_required

"""
Django 数据库操作
1.pip3 install  psycopg2-binary (注意:1.虚拟环境, 2.本次试验使用PSQL, 3.mysql安装mysqlclient)
2.python manage.py startapp qytdb
3.settings.py设置INSTALLED_APPS
        INSTALLED_APPS = [
            'django.contrib.admin',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'qytdb'+++
        ]
4.python manage.py check
5.settings.py设置PSQL数据库
        DATABASES = {
            'default': {
                # 'ENGINE': 'django.db.backends.sqlite3',
                # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'qytangdb',  # 数据库名称
                'USER': 'qytangdbuser',  # 拥有者，这个一般没修改
                'PASSWORD':'Cisc0123',  # 密码，自己设定的
                'HOST':'10.1.1.81',  # PSQL服务器地址
                'PORT':'',
            }
        }  
6.在models.py内编写模型类
7.python manage.py makemigrations qytdb
8.python manage.py migrate            
"""

"""
可能出现的问题
1.数据库没有auth_user表!
    删除数据图的所有条目
    重新makemigration, migrate
2.URL映射
    accounts/login
    accounts/logout

用户权限控制参考文档
https://blog.igevin.info/posts/django-permission/
https://www.jianshu.com/p/98ef9ca22b12

重要内容:
Django用permission对象存储权限项，每个model默认都有三个permission，即add model, change model和delete model。例如，定义一个名为『Car』model，定义好Car之后，会自动创建相应的三个permission：add_car, change_car和delete_car。Django还允许自定义permission，例如，我们可以为Car创建新的权限项：drive_car, clean_car, fix_car等等
需要注意的是，permission总是与model对应的，如果一个object不是model的实例，我们无法为它创建/分配权限

下面的操作都可以在后台管理执行

基本操作记录
myuser.groups.set([group_list])
myuser.groups.add(group, group, ...)
myuser.groups.remove(group, group, ...)
myuser.groups.clear()
myuser.user_permissions.set([permission_list])
myuser.user_permissions.add(permission, permission, ...)
myuser.user_permissions.remove(permission, permission, ...)
myuser.user_permissions.clear()
myuser.get_all_permissions()
myuser.get_group_permissions()

删除用户
user = User.objects.get(username='cq_bomb')
user.delete()

删除组
group = Group.objects.get(name='view_students_group')
group.delete()

组中删除用户
g.user_set.remove(your_user)

完整的创建用户,给予权限,删除的过程
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
permission = Permission.objects.get(codename='view_studentsdb')
user.user_permissions.add(permission)
user.get_all_permissions()
{'qytdb.view_studentsdb'}
user.has_perm('qytdb.view_studentsdb')
user = User.objects.get(username='collinsctk')
user.delete()

完整的创建用户/组,用户加入组,给予组权限,删除用户/组的过程
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
view_students_group,created = Group.objects.get_or_create(name='view_students_group')
user.groups.add(view_students_group)
permission = Permission.objects.get(codename='view_studentsdb')
view_students_group.permissions.add(permission)
user.get_all_permissions()
{'qytdb.view_studentsdb'}
user.has_perm('qytdb.view_studentsdb')
True
user.delete()
(2, {'admin.LogEntry': 0, 'auth.User_groups': 1, 'auth.User_user_permissions': 0, 'auth.User': 1})
view_students_group.delete()
(2, {'auth.Group_permissions': 1, 'auth.User_groups': 0, 'auth.Group': 1})


设置权限集合
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
students_group,created = Group.objects.get_or_create(name='students_group')
user.groups.add(students_group)
view = Permission.objects.get(codename='view_studentsdb')
add = Permission.objects.get(codename='add_studentsdb')
change = Permission.objects.get(codename='change_studentsdb')
students_group.permissions.set([view,add,change])
user.get_all_permissions()
{'qytdb.change_studentsdb', 'qytdb.view_studentsdb', 'qytdb.add_studentsdb'}

students_group.permissions.remove(add)
students_group.permissions.add(add)
注意:user.get_all_permissions()查询的只是缓存,不会变化
"""
# 展示所有学员信息的页面

@permission_required('qytdb.view_studentsdb')
def showstudents(request):
    # 查询整个数据库的信息 object.all()
    result = StudentsDB.objects.all()
    # 最终得到学员清单students_list,清单内部是每一个学员信息的字典
    students_list = []
    for x in result:
        # 产生学员信息的字典
        students_dict = {}
        # 为了不在模板中拼接字符串,提前为删除和编辑页面产生URI
        students_dict['id_delete'] = "/deletestudent/" + str(x.id) + "/"
        students_dict['id_edit'] = "/editstudent/" + str(x.id) + "/"

        # 提取学员详细信息,并写入字典
        students_dict['id'] = x.id
        students_dict['name'] = x.name
        students_dict['phone_number'] = x.phone_number
        students_dict['qq_number'] = x.qq_number
        students_dict['mail'] = x.mail
        students_dict['direction'] = x.direction
        students_dict['class_adviser'] = x.class_adviser
        students_dict['payed'] = x.payed
        students_dict['date'] = x.date
        students_list.append(students_dict)
    return render(request, 'showstudents.html', {'students_list': students_list})


# 删除特定ID的学员信息

@permission_required('qytdb.delete_studentsdb')
def deletestudent(request, id):
    m = StudentsDB.objects.get(id=id)
    m.delete()
    return HttpResponseRedirect('/dbshowstudents/')


# 添加学员信息函数

@permission_required('qytdb.add_studentsdb')
def addstudent(request):
    if request.method == 'POST':
        form = StudentsForm(request.POST)
        # 如果请求为POST,并且Form校验通过,把新添加的学员信息写入数据库
        if form.is_valid():
            s1 = StudentsDB(name=request.POST.get('name'),
                            phone_number=request.POST.get('phone_number'),
                            qq_number=request.POST.get('qq_number'),
                            mail=request.POST.get('mail'),
                            direction=request.POST.get('direction'),
                            class_adviser=request.POST.get('class_adviser'),
                            payed=request.POST.get('payed'))
            s1.save()
            # 写入成功后,重定向返回展示所有学员信息的页面
            return HttpResponseRedirect('/dbshowstudents/')
        else:  # 如果Form校验失败,返回客户在Form中输入的内容和报错信息
            return render(request, 'addstudent.html', {'form': form})
    else:  # 如果不是POST,就是GET,表示为初始访问, 显示表单内容给客户
        form = StudentsForm()
        return render(request, 'addstudent.html', {'form': form})


# 下面的函数是为了提取特定ID学员的详细信息,便于在编辑页面作为初始值进行展示


def getstudentinfo(id):
    # 设置过滤条件,获取特定学员信息, objects.get(id=id)
    result = StudentsDB.objects.get(id=id)
    students_dict = {}
    students_dict['id'] = result .id
    students_dict['name'] = result .name
    students_dict['phone_number'] = result .phone_number
    students_dict['qq_number'] = result .qq_number
    students_dict['mail'] = result .mail
    students_dict['direction'] = result .direction
    students_dict['class_adviser'] = result .class_adviser
    students_dict['payed'] = result .payed
    students_dict['date'] = result .date
    # 返回特定学员详细信息
    return students_dict


@permission_required('qytdb.change_studentsdb')
def editstudent(request, id):
    # 首先获取特定ID学员详细信息
    infodict = getstudentinfo(id)
    if request.method == 'POST':
        form = EditStudents(request.POST)
        # 如果请求为POST,并且Form校验通过,把修改过的学员信息写入数据库
        if form.is_valid():
            m = StudentsDB.objects.get(id=id)
            m.name = request.POST.get('name')
            m.phone_number = request.POST.get('phone_number')
            m.qq_number = request.POST.get('qq_number')
            m.mail = request.POST.get('mail')
            m.direction = request.POST.get('direction')
            m.class_adviser = request.POST.get('class_adviser')
            m.payed = request.POST.get('payed')
            m.save()
            # 写入成功后,重定向返回展示所有学员信息的页面
            return HttpResponseRedirect('/dbshowstudents/')
        else:  # 如果Form校验失败,返回客户在Form中输入的内容和报错信息
            return render(request, 'editstudent.html', {'form': form})
    else:  # 如果不是POST,就是GET,表示为初始访问, 把特定ID客户在数据库中的值,通过初始值的方式展现给客户看
        form = EditStudents(initial={'id': infodict['id'], # initial填写初始值
                                     'name': infodict['name'],
                                     'phone_number': infodict['phone_number'],
                                     'qq_number': infodict['qq_number'],
                                     'mail': infodict['mail'],
                                     'direction': infodict['direction'],
                                     'class_adviser': infodict['class_adviser'],
                                     'payed': infodict['payed']})
        return render(request, 'editstudent.html', {'form': form})


# 访问URI直接插入数据测试
def InsertData(request):
    # 插入
    s1 = StudentsDB(name='秦柯',
                    phone_number=13911153335,
                    qq_number=666658506,
                    mail='collinsctk@qytang.com',
                    direction='安全',
                    class_adviser='小雪',
                    payed='已缴费')
    s1.save()

    return HttpResponse('数据修改成功')
