#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a
from django.shortcuts import render
from datetime import datetime
from data import courses_db
from qytdb.models import Courses
import json

def summary(request):
    qytsummary = '课程摘要'
    # mytime = int(datetime.now().strftime('%w'))
    mytime = int(datetime.strptime('2018-08-06', '%Y-%m-%d').strftime('%w'))
    courses = Courses.objects.values('courses_name')
    courses_teachers = Courses.objects.values_list('courses_name', 'courses_teacher')
    courses_list = []
    teacher_list = []
    for c in courses:
        courses_list.append(c['courses_name'])
    for c_t in courses_teachers:
        teacher_list.append({'courses': c_t[0], 'teacher': c_t[1]})

    # courses_list = ['安全', '数据中心', '路由交换', '教主VIP']
    # teacher_list = [{'courses': '安全', 'teacher': '现任明教教主'},
    #                 {'courses': '数据中心', 'teacher': '马海波'},
    #                 {'courses': '路由交换', 'teacher': '安德'},
    #                 {'courses': '教主VIP', 'teacher': '现任明教教主'},
    #                 ]
    return render(request, 'summary.html', locals())


def sec_course(request):
    c = Courses.objects.get(courses_name='安全')

    courses_sec = {'方向': c.courses_name,
                   '摘要': c.courses_summary,
                   '授课老师': c.courses_teacher,
                   '授课方式': c.courses_method,
                   '课程特色': c.courses_characteristic,
                   '试验环境': c.courses_provide_lab,
                   '具体课程': json.loads(c.courses_detail)}
    print(courses_sec)
    return render(request, 'course.html', {'courseinfo': courses_sec})


def dc_course(request):
    c = Courses.objects.get(courses_name='数据中心')

    courses_dc = {'方向': c.courses_name,
                  '摘要': c.courses_summary,
                  '授课老师': c.courses_teacher,
                  '授课方式': c.courses_method,
                  '课程特色': c.courses_characteristic,
                  '试验环境': c.courses_provide_lab,
                  '具体课程': json.loads(c.courses_detail)}
    return render(request, 'course.html', {'courseinfo': courses_dc})