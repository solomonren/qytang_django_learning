#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a


from django.shortcuts import render
from data.fusioncharts import FusionCharts
import json
from random import randint
from django.contrib.auth.decorators import login_required, permission_required


# 提供charts测试主页,不推荐的iframe页面刷新
@login_required
def charts(request):
    # 提供pie部分的静态数据
    colors = json.dumps(['#007bff', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d'])
    pie_labels1 = ['路由交换','安全', '数据中心', '教主VIP', '无线', '华为']
    pie_data1 = json.dumps([10, 82, 50, 16, 70, 33])
    return render(request, 'charts.html', locals())


# 提供charts测试主页,推荐的iframe JSON刷新数据
@login_required
def chartsjson(request):
    # 提供pie部分的静态数据
    colors = json.dumps(['#007bff', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d'])
    pie_labels1 = ['路由交换','安全', '数据中心', '教主VIP', '无线', '华为']
    pie_data1 = json.dumps([10, 82, 50, 16, 70, 33])
    return render(request, 'charts_json.html', locals())

@login_required
def charts_update(request, chart_type, period, deviceid):
    # 暂时只支持line和bar两种chart类型
    if chart_type == 'line' or chart_type == 'bar':
        # 提供初始数据,刷新周期,和刷新页面的URL
        colors = json.dumps(['#007bff', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d'])
        labels = json.dumps(['2018-8-1','2018-8-2', '2018-8-3', '2018-8-4', '2018-8-5', '2018-8-6'])
        data1 = json.dumps([randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100)])
        data2 = json.dumps([randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100)])
        chart_type = chart_type
        period_1000 = int(period) * 1000
        update_url = '/chartsdata/'+chart_type+'/'+str(deviceid)+'/'
        return render(request, 'charts_update.html', locals())

@login_required
def charts_update_json(request, chart_type, period, deviceid):
    # 暂时只支持line和bar两种chart类型
    if chart_type == 'line' or chart_type == 'bar':
        # 提供初始数据,刷新周期,和JSON刷新数据页面的URL
        colors = json.dumps(['#007bff', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d'])
        labels = json.dumps(['2018-8-1', '2018-8-2', '2018-8-3', '2018-8-4', '2018-8-5', '2018-8-6'])
        data1 = json.dumps([randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100)])
        data2 = json.dumps([randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100)])
        chart_type = chart_type
        period_1000 = int(period) * 1000
        update_url = '/chartjson/'+chart_type+'/'+str(deviceid)+'/'
        return render(request, 'charts_update_json.html', locals())


# 提供刷新页面内容(不推荐刷新页面方式)
@login_required
def charts_data(request, chart_type, deviceid):
    if chart_type == 'line' or chart_type == 'bar':
        colors = json.dumps(['#007bff', '#28a745', '#333333', '#c3e6cb', '#dc3545', '#6c757d'])
        labels = json.dumps(['2018-8-1','2018-8-2', '2018-8-3', '2018-8-4', '2018-8-5', '2018-8-6'])
        data1 = json.dumps([randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100)])
        data2 = json.dumps([randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100), randint(1,100)])
        chart_type = chart_type
        return render(request, 'charts_data.html', locals())


# 提供FusionChart页面
@login_required
def fusioncharts(request):
    chart_data1 = {
                  "chart": {
                            "caption": "Router CPU Usage",
                            "yaxisname": "percent",
                            "subcaption": "[2018-8-6]",
                            "numbersuffix": "%",
                            "rotatelabels": "1",
                            "setadaptiveymin": "1",
                            "theme": "fusion"
                            },
                  "data": [
                            {
                             "label": "00:00",
                             "value": "89.45"
                            },
                            {
                             "label": "04:00",
                             "value": "89.87"
                            },
                            {
                             "label": "08:00",
                             "value": "89.64"
                            },
                            {
                             "label": "12:00",
                             "value": "90.13"
                            },
                            {
                             "label": "16:00",
                             "value": "90.67"
                            },
                            {
                             "label": "20:00",
                             "value": "90.54"
                            }
                        ]
                }
    column1d = FusionCharts(
        'line',
        'ex1',
        '600',
        '400',
        'chart-1',
        'json',
        json.dumps(chart_data1),
    )

    chart_data2 = {
                    "chart": {
                        "caption": "Countries with Highest Deforestation Rate",
                        "subcaption": "For the year 2017",
                        "yaxisname": "Deforested Area{br}(in Hectares)",
                        "decimals": "1",
                        "theme": "fusion"
                    },
                    "data": [
                        {
                            "label": "Brazil",
                            "value": "1466000"
                        },
                        {
                            "label": "Indonesia",
                            "value": "1147800"
                        },
                        {
                            "label": "Russian Federation",
                            "value": "532200"
                        },
                        {
                            "label": "Mexico",
                            "value": "395000"
                        },
                        {
                            "label": "Papua New Guinea",
                            "value": "250200"
                        },
                        {
                            "label": "Peru",
                            "value": "224600"
                        },
                        {
                            "label": "U.S.A",
                            "value": "215200"
                        },
                        {
                            "label": "Bolivia",
                            "value": "135200"
                        },
                        {
                            "label": "Sudan",
                            "value": "117807"
                        },
                        {
                            "label": "Nigeria",
                            "value": "82000"
                        }
                    ]
                }
    column2d = FusionCharts(
        'column3d',
        'ex2',
        '600',
        '400',
        'column-1',
        'json',
        json.dumps(chart_data2),
    )

    return render(request, 'fusioncharts.html',{'output1': column1d.render(), 'output2': column2d.render()})