from django.db import models
from django.core.validators import RegexValidator

"""
Django Model Data Types介绍:
https://www.webforefront.com/django/modeldatatypesandvalidation.html

Model field reference:
https://docs.djangoproject.com/zh-hans/2.1/ref/models/fields/

添加后修改Model后需要自行如下操作:
python manage.py makemigrations
python manage.py migrate

使用PSQL图形管理工具pgAdmin4查看数据库
Databases---qytangdb---Schemas---public---Tables---qytdb_studentsdb---右键View/Edit Data---All Rows
"""

"""
可能出现的问题
1.数据库没有auth_user表!
    删除数据图的所有条目
    重新makemigration, migrate
2.URL映射
    accounts/login
    accounts/logout

用户权限控制参考文档
https://blog.igevin.info/posts/django-permission/
https://www.jianshu.com/p/98ef9ca22b12

重要内容:
Django用permission对象存储权限项，每个model默认都有三个permission，即add model, change model和delete model。例如，定义一个名为『Car』model，定义好Car之后，会自动创建相应的三个permission：add_car, change_car和delete_car。Django还允许自定义permission，例如，我们可以为Car创建新的权限项：drive_car, clean_car, fix_car等等
需要注意的是，permission总是与model对应的，如果一个object不是model的实例，我们无法为它创建/分配权限

下面的操作都可以在后台管理执行

基本操作记录
myuser.groups.set([group_list])
myuser.groups.add(group, group, ...)
myuser.groups.remove(group, group, ...)
myuser.groups.clear()
myuser.user_permissions.set([permission_list])
myuser.user_permissions.add(permission, permission, ...)
myuser.user_permissions.remove(permission, permission, ...)
myuser.user_permissions.clear()
myuser.get_all_permissions()
myuser.get_group_permissions()

删除用户
user = User.objects.get(username='cq_bomb')
user.delete()

删除组
group = Group.objects.get(name='view_students_group')
group.delete()

组中删除用户
g.user_set.remove(your_user)

完整的创建用户,给予权限,删除的过程
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
permission = Permission.objects.get(codename='view_studentsdb')
user.user_permissions.add(permission)
user.get_all_permissions()
{'qytdb.view_studentsdb'}
user.has_perm('qytdb.view_studentsdb')
user = User.objects.get(username='collinsctk')
user.delete()

完整的创建用户/组,用户加入组,给予组权限,删除用户/组的过程
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
view_students_group,created = Group.objects.get_or_create(name='view_students_group')
user.groups.add(view_students_group)
permission = Permission.objects.get(codename='view_studentsdb')
view_students_group.permissions.add(permission)
user.get_all_permissions()
{'qytdb.view_studentsdb'}
user.has_perm('qytdb.view_studentsdb')
True
user.delete()
(2, {'admin.LogEntry': 0, 'auth.User_groups': 1, 'auth.User_user_permissions': 0, 'auth.User': 1})
view_students_group.delete()
(2, {'auth.Group_permissions': 1, 'auth.User_groups': 0, 'auth.Group': 1})


设置权限集合
user = User.objects.create_user('collinsctk', 'collinsctk@qytang.com', 'Cisc0123')
user.save()
students_group,created = Group.objects.get_or_create(name='students_group')
user.groups.add(students_group)
view = Permission.objects.get(codename='view_studentsdb')
add = Permission.objects.get(codename='add_studentsdb')
change = Permission.objects.get(codename='change_studentsdb')
students_group.permissions.set([view,add,change])
user.get_all_permissions()
{'qytdb.change_studentsdb', 'qytdb.view_studentsdb', 'qytdb.add_studentsdb'}

students_group.permissions.remove(add)
students_group.permissions.add(add)
注意:user.get_all_permissions()查询的只是缓存,不会变化
"""


class Courses(models.Model):
    courses_name = models.CharField(max_length=100, blank=False)
    courses_summary = models.CharField(max_length=10000, blank=False)
    courses_teacher = models.CharField(max_length=100, blank=False)
    courses_method = models.CharField(max_length=100, blank=False)
    courses_characteristic = models.CharField(max_length=100, blank=True)
    courses_provide_lab = models.CharField(max_length=100, blank=False)
    courses_detail = models.CharField(max_length=10000, blank=False)


class StudentsDB(models.Model):
    # 名字,最大长度50,可以为空 (注意:并没有min_length这个控制字段)
    name = models.CharField(max_length=50, blank=False)

    # 电话号码,校验以1开头的11位数字,最大长度为11,不可以为空,唯一键(注意:并没有min_length这个控制字段)
    phone_regex = RegexValidator(regex=r'^1\d{10}$',
                                 message="Phone number must be entered in the format: '13911153335'. 11 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=11, blank=False, unique=True)

    # QQ号,校验5到20位的数字,最大长度为20,不可以为空,唯一键(注意:并没有min_length这个控制字段)
    qq_regex = RegexValidator(regex=r'^\d{5,20}$',
                              message="QQ number must be entered in the format: '605658506'. 5-20 digits allowed.")
    qq_number = models.CharField(validators=[qq_regex], max_length=20, blank=False, unique=True)

    # 邮件,EmailField会校验邮件格式,最大长度50, 可以为空(注意:并没有min_length这个控制字段)
    mail = models.EmailField(max_length=50, blank=True)

    # 学习方向,后面的为选择内容,前面为写入数据库的值, 注意max_length必须配置
    direction_choices = (('安全', '安全'), ('教主VIP', '教主VIP'))
    direction = models.CharField(max_length=5, choices=direction_choices)

    # 班主任,后面的为选择内容,前面为写入数据库的值, 注意max_length必须配置
    class_adviser_choices = (('小雪', '小雪'), ('菲儿', '菲儿'))
    class_adviser = models.CharField(max_length=2, choices=class_adviser_choices)

    # 缴费情况,后面的为选择内容,前面为写入数据库的值, 注意max_length必须配置
    payed_choices = (('已缴费', '已缴费'), ('未交费', '未交费'))
    payed = models.CharField(max_length=3, choices=payed_choices)

    # 报名日期,自动添加日期项
    date = models.DateField(auto_now_add=True)